from django.db import models

class TextMessage(models.Model):
    text = models.CharField(max_length=200)
    author = models.CharField(max_length=20)
    recipient = models.CharField(max_length=20)

class ImageMessage(models.Model):
    image_url = models.CharField(max_length=200)
    author = models.CharField(max_length=20)
    recipient = models.CharField(max_length=20)
