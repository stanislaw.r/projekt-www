from django.contrib import admin

from .models import Question, Choice, Answer


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'pub_date', 'type', 'description')
    list_filter = ('question_text', 'pub_date', 'type', )
    search_fields = ('question_text',)

    def description(self, obj):
        return obj.__str__()

@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'choice_text', 'votes', )
    list_filter = ('question', 'choice_text', 'votes', )
    search_fields = ('choice_text', )

    def question_text(self, obj):
        return obj.question.question_text

@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'answer_text', )
    list_filter = ('question', 'answer_text', )
    search_fields = ('answer_text', )

    def question_text(self, obj):
        return obj.question.question_text