from django import forms
from django.forms.widgets import DateInput

from .models import Question, Choice


class QuestionForm(forms.ModelForm):
   class Meta:
       model = Question
       fields = '__all__' #['choice_text', 'votes']
       widgets = {
           'pub_date': DateInput(attrs={'type': 'date'}),
       }


class ChoiceForm(forms.ModelForm):
   class Meta:
       model = Choice
       fields = '__all__'
       widgets = {
            'question': forms.HiddenInput()
       }