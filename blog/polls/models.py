from django.db import models


class Question(models.Model):
    OTWARTE = "OT"
    ZAMKNIETE = "ZA"

    TYPE_CHOICES=[
        (OTWARTE, "Otwarte"),
        (ZAMKNIETE, "Zamkniete"),
    ]

    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    type = models.CharField(max_length=2, choices=TYPE_CHOICES, default=OTWARTE)

    def __str__(self):
        return f"Pytanie {self.question_text}"



class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='choices')
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.CharField(max_length=200)
